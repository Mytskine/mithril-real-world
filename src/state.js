const state = {
    deck: {
        cards: [], // string[]
        removed: new Set(),
        turned: [],
    },
    lock: false,
    pairings: new Map(), // card (string) to group (array)
    waitTime: 2000,
}

/**
 * Cards defined in state.pairings are shuffled and stored in state.deck.cards.
 */
function shuffleCards() {
    state.deck.cards = []
    for (let card of state.pairings.keys()) {
        state.deck.cards.push(card)
    }
    for (let i = state.deck.cards.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1))
        const tmp = state.deck.cards[j]
        state.deck.cards[j] = state.deck.cards[i]
        state.deck.cards[i] = tmp
    }
}

/**
 * Fill state.pairings and state.cards.
 */
function setPairings(groups) {
    state.pairings = new Map()
    state.deck.cards = []
    for (let g of groups) {
        for (let card of g) {
            if (typeof card !== 'string') {
                console.log("Card is not a string:", card)
                return false
            }
            if (state.pairings.has(card)) {
                console.log("Card is not unique:", card)
                return false
            }
            state.pairings.set(card, g)
            state.deck.cards.push(card)
        }
    }
    return true
}

function getCardStatus(card) {
    if (state.deck.removed.has(card)) {
        return "removed";
    }
    if (state.deck.turned.includes(card)) {
        return "turned";
    }
    return "back";
}

function getCards() {
    return state.deck.cards
}

function isLocked() {
    return state.lock
}

function restart() {
    state.deck.cards = []
    state.deck.removed = new Set()
    state.deck.turned = []
}

function getWaitTime() {
    return state.waitTime
}
function setWaitTime(ms) {
    state.waitTime = ms
}

function turnCardOver(card) {
    state.deck.turned.push(card)
}

function testCardsTurnedOver(card) {
    if (state.deck.turned.length <= 1) {
        return
    }
    const firstCard = state.deck.turned[0]
    const pairing = state.pairings.get(firstCard)
    const lastCard = state.deck.turned[state.deck.turned.length - 1]
    if (state.pairings.get(lastCard) !== pairing) {
        // cards do not match
        state.deck.turned = []
        return
    }
    if (pairing.length === state.deck.turned.length) {
        // we have a full pairing
        for (const c of state.deck.turned) {
            state.deck.removed.add(c)
        }
        state.deck.turned = []
        return
    }
    // incomplete pairing
}

export {
    getCards,
    getCardStatus,
    getWaitTime,
    restart,
    setPairings,
    setWaitTime,
    shuffleCards,
    testCardsTurnedOver,
    turnCardOver,
}
