import m from "mithril"
import {Deck} from "./deck"

const Game = {
    view: function(vnode) {
        /* TODO: add other components, e.g.:
         * - message when no cards are left
         * - size of the pairing for the first card
         * - time elapsed
         */
        return m("div.game",
            m(Deck)
        )
    }
}

export {
    Game
}
