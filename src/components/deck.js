import m from "mithril"
import * as state from "../state"

const Deck = {
    view: function(vnode) {
        const cards = state.getCards()
        return m("div.deck",
            cards.map(function(content, index) {
                return m(Card, {content, index})
            })
        )
    }
}

const Card = {
    view: function(vnode) {
        const content = vnode.attrs.content
        const status = state.getCardStatus(content)
        const wait = state.getWaitTime()
        return m("div.card.index-" + vnode.attrs.index, {
                class: status,
                onclick: function() {
                    if (status !== "back") {
                        return
                    }
                    state.turnCardOver(content)
                    if (wait > 0) {
                        setTimeout(function() {
                            state.testCardsTurnedOver()
                            m.redraw()
                        }, 2000)
                    } else {
                        state.testCardsTurnedOver()
                    }
                },
            }, m("span", m.trust(content))
        )
    }
}

export {
    Deck
}
