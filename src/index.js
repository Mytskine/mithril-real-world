import m from "mithril"
import {setPairings, shuffleCards} from "./state"
import {Game} from "./components/game"

window.Memory = {
    init: function (container, initialData) {
        setPairings(initialData)
        shuffleCards()
        return m.mount(container, Game)
    }
}
