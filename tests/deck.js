const assert = require("assert");
const {Deck} = require("../src/components/deck")
const state = require("../src/state")

function initState() {
    state.restart()

    // Do not shuffle the cards!
    state.setPairings([["a", "A", "à"], ["c", "ç"], ["oui", "si"]])

    // For faster and easier tests, do no wait while showing the cards
    state.setWaitTime(0)
}

describe('Deck', function() {
    it('should contain n cards', function() {
        initState()
        var output = mq(Deck)
        output.should.have(7, '.card.back')
    })

    it('should hide matching cards', function() {
        initState()
        var output = mq(Deck)
        output.click('.card.index-6')
        output.should.have(6, '.card.back')
        output.should.have(1, '.card.turned')

        output.click('.card.index-5')
        output.should.have(5, '.card.back')
        output.should.not.have('.card.turned')
        output.should.have(2, '.card.removed')
    })

    it('should restore unmatching cards', function() {
        initState()
        var output = mq(Deck)
        output.should.have(7, '.card.back')

        output.click('.card.index-0')
        output.should.have(6, '.card.back')
        output.should.have(1, '.card.turned')

        output.click('.card.index-6')
        output.should.not.have('.card.turned')
        output.should.not.have('.card.removed')
        output.should.have(7, '.card.back')
    })
})
