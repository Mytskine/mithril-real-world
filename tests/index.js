/* global global, process */

// Hide the console.log messages unless the test failed.
let log = console.log;
let output = [];
beforeEach(function () {
    console.log = function (...args) {
        let row = "";
        for (let x of args) {
            row += JSON.stringify(x, null, 4) + "\n";
        }
        output.push(row);
    };
});
afterEach(function () {
    console.log = log;
    if (this.currentTest.state === 'failed' && output.length > 0) {
        process.stderr.write("## console log: START\n");
        process.stderr.write(output.join("\n"));
        process.stderr.write("## console log: END\n");
    }
    output = [];
});

// Prepare the Mithril test environment that emulates a browser.
global.window = Object.assign(
    require('mithril/test-utils/domMock.js')(),
    require('mithril/test-utils/pushStateMock')()
);
global.requestAnimationFrame = callback =>
    global.setTimeout(callback, 1000 / 60);

// cannot `import` since we have to wait for the global `window`
global.mq = require('mithril-query');

require('./deck');
