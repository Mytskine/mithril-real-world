Testing a Mithril app with Mocha
================================

Supposing the application is [configured to *bundle* with Webpack](mithril-webpack.md),
we now want to write automated tests.


Installing and configuring
--------------------------

To test Mithril, we need a specialized library, [mithril-query](https://github.com/MithrilJS/mithril-query),
and a generic testing framework which could be anything, here [Mocha](https://mochajs.org/).
```sh
yarn add --dev mithril mithril-query mocha webpack-node-externals
```

We must configure *webpack* so that it can build a bundle with all the tests
 that *mocha* will run.
Since *Webpack* is by default meant for the browser,
we need a way to tell it to target a Node.js environnment,
since this is where *mocha* lives.
```javascript
// webpack-test.config.js 

const path = require('path');

module.exports = {
    entry: './tests/index.js',
    output: {
        filename: 'tests.js',
        path: path.resolve(__dirname, 'dist'),
    },
    target: 'node', // webpack 4
    externalsPresets: { // webpack 5, remove with webpack 4
        node: true,
    },
    externals: [
		// see https://github.com/liady/webpack-node-externals
        require('webpack-node-externals')(),
    ],
};
```

Now `yarn webpack -c webpack-test.config.js` will produce a file `dist/tests.js`
that we can evaluate with `yarn mocha dist/tests.js`.
But there's a simple way to chain these commands by appendingthis block to `package.json`:
```json
  "scripts": {
    "build": "webpack build --mode=production",
    "pretest": "webpack build -c webpack-test.config.js --mode=none",
    "test": "mocha --colors=0 dist/tests.js"
  }
```


A minimal test suite
--------------------

The [documentation of mithril-query](https://github.com/MithrilJS/mithril-query#usage)
provides an example of a simple test.

```javascript
// src/simple.js
const m = require('mithril')
module.exports = {
    view: function() {
        return m('div', [
            m('span', 'spanContent'),
            m('#fooId', 'fooContent'),
            m('.barClass', 'barContent'),
        ])
    },
}
```

```javascript
// tests/index.js
global.window = Object.assign(
  require('mithril/test-utils/domMock.js')(),
  require('mithril/test-utils/pushStateMock')()
)
global.requestAnimationFrame = callback =>
  global.setTimeout(callback, 1000 / 60)

const simpleModule = require('./simple')
const mq = require('../')

describe('simple module', function() {
    it('should generate appropriate output', function() {
        var output = mq(simpleModule)
        output.should.have('span')
    })
})
```

```sh
yarn test
```

Extending from this, it's easy to write tests in files which the entry-point will `require()`.
See [../tests/] for a realistic test suite.


Everyday testing
----------------

With the config described above, tests are run with:
```sh
yarn test
# or
npm test
```

You can also run tests each time a file is changed,
by watchnig the source files with webpack,
and watching the test bundle with mocha:
```
yarn mocha --watch --watch-files tests/index.js --colors=0 tests/index.js
# In a distinct terminal (another tab, another window, GNU Screen...)
yarn webpack build -c webpack-test.config.js --mode=none --watch
```


To write tests, you will need:

- the [mithril-query API](https://github.com/MithrilJS/mithril-query#api),
- the [Mocha documentation](https://mochajs.org),
- an assertion module, like the default Node.js [assert](https://nodejs.org/api/assert.html),
  or one among [others suggested by Mocha](https://mochajs.org/#assertions)
  (beware, some of those are not active anymore).

For most cases, it is enough to know the dozen of functions from *mithril-query*,
the functions `describe()` and `it()` from *mocha*,
and a few of the functions from *assert* (mostly `deepEqual()`, `ok()` and `strictEqual()`).

See [../tests/] for an example of realistic testing.


Automating with Gitlab CI
-------------------------

See [.gitlab-ci.yml](.gitlab-ci.yml) for a way to run the test suite each time the code is pushed into Gitlab.

The process is similar with other CI tools (Github actions, CircleCI…).


Annex: problems along the way
-----------------------------

You may encounter the same problems, but beware:
a similar message does not mean the error is the same.


### Webpack5, critical error about polyfills

Webpack wants to bundle for the web,
and can't do it (unless configured to) because the entry-point uses Node.js modules.

**Solution:**
Configure a "node" target.
For Webpack5, the recommended way is through `externalsPresets: { node: true }`,
see above.


### Webpack, many warnings about "Critical dependency"

The expected bundle size was 20 Kb, but `webpack` produced a 8 Mb file,
and display half a dozen messages like
```
WARNING in ./node_modules/prettier/index.js 9703:17-68
Critical dependency: the request of a dependency is an expression
```

**Solution:**
Add the plugin `webpack-node-externals`
and its configuration in the `externals` block (see above).


### Tests complain that `m` is not available

The mithril `m` is declared in the test environment, as suggested by *mithril-query,
but that does not extend to everywhere in the source.
If your code expects a distinct bundle for `mithril.js`, it won't load `m` by itself.

**Solution:**
Add `m` as a global variable.
```javascript
// In tests/index.js:
global.m = require('mithril');
```

### Mocha test failed without a calltrace

My first real Mocha test was failing,
but I didn't where since there was no stack trace,
just a few lines of blank space under the error...
Turns out Mocha dislikes dark terminals:
ignoring my black background, the trace was written with a black foreground color.

**Solution:**
Disable (partly) the colored output with the `--colors=0` option:
`yarn mocha --colors=0 dist/tests.js`

### Error with `indexOf` inside Mithril code

```
TypeError: Cannot read property 'indexOf' of undefined
      at parseURL (node_modules/mithril/test-utils/parseURL.js:5:26)
```
This appeared when using the [History API](https://developer.mozilla.org/en-US/docs/Web/API/History).

**Solution:**
I chose to disable calls to `window.history.pushState()` when testing,
with a dumb detection of Mocha:
```javascript
if (typeof global === 'undefined' && !('specify' in global)) {
    // probably not running in a test env
    window.history.pushState(state, title);
}
```

### Syntax error with `should.contain()`

```
SyntaxError: Bad combinator
 at compile (node_modules/domino/lib/select.js:714:35)
```
This araised from a seemingly innocuous `output.should.contain("droits d'auteurs");`.
I didn't investigate if *mithril-query*'s function was at fault or if it was a dependency.

**Solution:**
Do not use `'` in strings tested.



License
-------
While the source code in this project is under a MIT license (you may copy freely),
this document is under *CC BY* (please mention your source).

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
</a>

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 Generic License</a>.
