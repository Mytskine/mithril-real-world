Mithril: bundling and testing
=============================

TOC
----
- [using a bundler](mithril-webpack.md) (Webpack)
- [using a test library](mithril-mocha) (Mocha)


Why? Dealing with complexity...
-------------------------------

For a very small application, a single JS file may be enough.
```javascript
// basic.js
function myApp(container, initialData) {
    let state = {};
    const Main = {
        view: function() {
            return m("div", {
                    onclick: function() {
                        state.num++;
                    },
                }, state.num);
        }
    };

    state = initialData;
    m.mount(container, Main);
};
```

This file is loaded from a HTML view, with the initial state possibly defined server-side:
```html
<!DOCTYPE html>
<html><!-- basic.html -->
    <head>
        <title>Basic mithril app</title>
        <script src="mithril.js"></script>
        <script src="basic.js"></script>
    </head>
    <body>
        <div id="container"></div>
        <script>
        addEventListener('onload', function() {
            // The server template system may fill the parameter, here {num: 14}.
            myApp(document.getElementById('container', {num: 14});
        });
        </script>
    </body>
</html>
```

But once the JS code grows, it becomes hard keeping it in a single file.
It's much easier to split the Mithril components across several files.
```javascript
import {readState} from './state'
import {Gauge, Progressbar} from './visual-misc'

const Main = {
    view: function(vnode) {
        const state = readState();
        return m("section",
            m("h2", "Working..."),
            m(Gauge, state.gauge),
            m(Progressbar, state.progressbar)
        );
    }
}
```

In order to achieve this, you need to [use a bundler](mithril-webpack.md).


License
-------
While the source code in this project is under a MIT license (you may copy freely),
this document is under *CC BY* (please mention your source).

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
</a>

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 Generic License</a>.
