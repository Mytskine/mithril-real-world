Using Webpack with Mithril
==========================

[Webpack](https://webpack.js.org) is, by far, the most used of the JS bundler.
This tutorial will show how to build your JS application into a single *.js* file.

Install webpack
---------------

You should use a package manager, the classic `npm` or the newer `yarn`.

If the project does not have a file `package.json` to decribe it,
it's recommended to create it first, though it it not required.
In the project directory, `yarn init` will create it interactively.
Then install Webpack.

```sh
yarn add --dev webpack webpack-cli
# alternative, if using npm
npm install  webpack webpack-cli --save-dev
```

Configure
---------

Webpack can handle many formats (images, styles…),
but this minimal configuration just digests JS files.

```javascript
// webpack.config.js

const path = require('path');

module.exports = {
    // relative path to the entry-point
    entry: './src/index.js',

    // The entry point and its dependencies will be compiled into "./dist/index.js".
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist'),
    },
};
```

See [Webpack's documentation](https://webpack.js.org/concepts/) (concepts, configuration, guides)
if you want more than this basic feature.

Use webpack
-----------

Now webpack will produce the file `dist/index.js` with:
```sh
# optimised file (minified, etc...)
yarn webpack --mode=production
# plain file (mostly a concatenation)
yarn webpack --mode=none
# file that dinamically loads its dependencies
# (will work only if the filed is served with the right tools)
yarn webpack --mode=development
```

This file may or may not include the whole Mithril framework,
depending on the entry point `src/index.js`.
When serving multiple pages with distinct apps,
it can be more efficient to serve separately `mithril.js` and keep it in the browser cache.
```javascript
// Uncomment the following line to include mithril in your bundle.
//import m from "mithril"
// Don't forget to add it to the dependencies with `yarn add --dev mithril` or similar.

// Since mithril is not imported, we may declare it to linters and IDEs.
/* global m */

// js code...
```

It's now time to write some code and [test it](mithirl-mocha.md).


License
-------
While the source code in this project is under a MIT license (you may copy freely),
this document is under *CC BY* (please mention your source).

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
</a>

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 Generic License</a>.
