Real world Mithril.js
=====================

This repository proposes 2 ways of improve your skills with Mithril:

- by reading the [tutorial](tutorial/),
- by reading the source of this application.

The tutorial intends to complete the official documentation where it is lacking.
It covers subjects about integrating the library in a JS system:

- [using a bundler](doc/mithril-webpack.md) (Webpack) to organize the Mithril code in many files,
- [using a test library](doc/mithril-mocha) (Mocha).

The final application is a simple memory game.
If you prefer reading the code, it may help to navigate to older commits
where the application was even simpler.


License
-------
While the source code in this project is under a MIT license (you may copy freely),
this document is under *CC BY* (please mention your source).

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
</a>

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 Generic License</a>.
