const path = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'memory.js',
        path: path.resolve(__dirname, 'dist'),
    },
};
