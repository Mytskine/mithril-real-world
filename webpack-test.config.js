const path = require('path');

module.exports = {
    entry: './tests/index.js',
    output: {
        filename: 'tests.js',
        path: path.resolve(__dirname, 'dist'),
    },
    target: 'node', // webpack 4
    externals: [
		// see https://github.com/liady/webpack-node-externals
        require('webpack-node-externals')(),
    ],
    externalsPresets: { // webpack 5, remove with webpack 4
        node: true,
    },
};
